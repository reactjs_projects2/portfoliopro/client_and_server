module.exports = {
    "extends": [
      "eslint:recommended",
      "plugin:react/recommended"
    ],
    "plugins": [
      "react"
    ],
    "settings": {
      "react": {
        "version": "detect" // Cette ligne détectera automatiquement la version de React
      }
    },
    "rules": {
    }
  }
  