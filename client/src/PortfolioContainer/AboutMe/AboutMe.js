import React from "react";
import PropTypes from "prop-types";
import ScreenHeading from "../../utilities/screenHeading/screenHeading";
import ScrollService from "../../utilities/ScrollService";
import "./AboutMe.css";

const AboutMe = ({ id }) => {
  const SCREEN_CONSTANTS = {
    description:
      "Je suis un développeur Full Stack passionné qui transforme des idées en expériences numériques exceptionnelles, aussi bien sur le web que sur mobile.",
    highlights: {
      bullets: [
        "Développement web et mobile Full Stack",
        "Front End interactif selon la conception",
        "Angular et ReactJs",
        "Flutter pour le developpement mobile",
        "Conception d'API REST avec Symfony",
        "Administration de BDD",
      ],
      heading: "Voici quelques points forts:",
    },
  };

  const renderHighlight = () => {
    return SCREEN_CONSTANTS.highlights.bullets.map((value, i) => (
      <div className="highlight" key={i}>
        <div className="highlight-blob"></div>
        <span>{value}</span>
      </div>
    ));
  };

  return (
    <div className="about-me-container screen-container fade-in" id={id}>
      <div className="about-me-parent">
        <ScreenHeading
          title={"A Propos de Moi"}
          subHeading={"Pourquoi me choisir?"}
        />
        <div className="about-me-card">
          <div className="about-me-profile"></div>
          <div className="about-me-details">
            <span className="about-me-description">
              {SCREEN_CONSTANTS.description}
            </span>
            <div className="about-me-highlights">
              <div className="highlight-heading">
                <span>{SCREEN_CONSTANTS.highlights.heading}</span>
              </div>
              {renderHighlight()}
            </div>
            <div className="about-me-options">
              <button
                className="btn primary-btn"
                onClick={() => ScrollService.scrollHandler.scrollToHireMe()}
              >
                Engagez moi
              </button>
              <a
                href="Comlan_Beh_Ardieu_Alotin_CV.pdf"
                download="Beh Comlan_Beh_Ardieu_Alotin_CV.pdf"
              >
                <button className="btn highlighted-btn">Obtenir mon CV</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

AboutMe.propTypes = {
  id: PropTypes.string.isRequired,
};

export default AboutMe;
