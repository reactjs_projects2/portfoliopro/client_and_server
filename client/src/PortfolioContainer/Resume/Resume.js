import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import ScreenHeading from "../../utilities/screenHeading/screenHeading";
import Animations from "../../utilities/Animations";
import "./Resume.css";
import ScrollService from "../../utilities/ScrollService";

const Resume = (props) => {
  /* STATES */
  const [selectedBulletIndex, setSelectedBulletIndex] = useState(0);
  const [carousalOffsetStyle, setCarousalOffsetStyle] = useState({});

  let fadeInScreenHandler = (screen) => {
    if (screen.fadeInScreen !== props.id) return;

    Animations.animations.fadeInScreen(props.id);
  };
  const fadeInSubscription =
    ScrollService.currentScreenFadeIn.subscribe(fadeInScreenHandler);

  /* REUSABLE MINOR COMPONENTS */
  const ResumeHeading = (props) => {
    return (
      <div className="resume-heading">
        <div className="resume-main-heading">
          <div className="heading-bullet"> </div>{" "}
          <span> {props.heading ? props.heading : ""} </span>{" "}
          {props.fromDate && props.toDate ? (
            <div className="heading-date">
              {" "}
              {props.fromDate + "-" + props.toDate}{" "}
            </div>
          ) : (
            <div> </div>
          )}{" "}
        </div>{" "}
        <div className="resume-sub-heading">
          <span> {props.subHeading ? props.subHeading : ""} </span>{" "}
        </div>{" "}
        <div className="resume-heading-description">
          <span> {props.description ? props.description : ""} </span>{" "}
        </div>{" "}
      </div>
    );
  };

  /* STATIC RESUME DATA FOR THE LABELS*/
  const resumeBullets = [
    { label: "Education", logoSrc: "education.svg" },
    { label: "Expériences", logoSrc: "work-history.svg" },
    {
      label: "Compétences",
      logoSrc: "programming-skills.svg",
    },
    { label: "Projets", logoSrc: "projects.svg" },
    { label: "Intérêts", logoSrc: "interests.svg" },
  ];

  //here we have
  const programmingSkillsDetails = [
    { skill: "Angular/TypeScript", ratingPercentage: 85 },
    { skill: "Flutter/Dart", ratingPercentage: 89 },
    { skill: "React/JavaScript", ratingPercentage: 85 },
    { skill: "Symfony/PHP", ratingPercentage: 80 },
    { skill: "SEO", ratingPercentage: 60 },
    { skill: "Node JS", ratingPercentage: 70 },
    { skill: "Mongo Db", ratingPercentage: 70 },
    { skill: "HTML", ratingPercentage: 80 },
    { skill: "CSS", ratingPercentage: 80 },
  ];

  const projectsDetails = [
    {
      title: "FoodPanda, apllication mobile semblable à Uber Eat ",
      duration: { fromDate: "2023", toDate: "En cours" },
      description:
        "Je travaille actuellement sur une application similaire à Uber Eats, non destinée à la commercialisation, mais créée pour m'entraîner et perfectionner mes compétences en développement mobile. Cette expérience me permet d'explorer les défis passionnants du monde des applications de livraison et de renforcer mon expertise",
      subHeading: "Technologies utilisées: Flutter, Firebase, CSS.",
    },
    {
      title: "Site Web de portfolio personnel",
      duration: { fromDate: "2022", toDate: "2023" },
      description:
        "Un site Web de portfolio personnel pour présenter tous mes détails et projets en un seul endroit.",
      subHeading: "Technologies utilisées: React JS, Bootsrap",
    },
    {
      title: "Site Web pour une agence de sécurité privée (GISA, Bénin)",
      duration: { fromDate: "2020", toDate: "2021" },
      description:
        "J'ai eu le privilège de développer un site web sur mesure pour une société de sécurité privée et événementielle, offrant ainsi une plateforme interactive qui présente leur expertise en sécurité tout en facilitant le recrutement de nouveaux agents qualifiés.",
      subHeading: "Technologies utilisées:  Laravel, HTML, CSS, Bootstrap, Js.",
    },
  ];

  const resumeDetails = [
    <div className="resume-screen-container" key="education">
      <ResumeHeading
        heading={"INstitut F2I, Vincennes"}
        subHeading={"Diplôme de Consultant Développeur Web et Mobile."}
        fromDate={"2022"}
        toDate={"2023"}
      />{" "}
      <ResumeHeading
        heading={"ENEAM, Bénin"}
        subHeading={
          "Diplôme de Technicien Supérieur en Informatique (Analyse Informatique et Programmation)."
        }
        fromDate={"2017"}
        toDate={"2021"}
      />{" "}
      <ResumeHeading
        heading={"CEG Le Méridien, Bénin"}
        subHeading={"BACCALAUREAT SERIE C"}
        fromDate={"2014"}
        toDate={"2017"}
      />{" "}
      <ResumeHeading
        heading={"CS Le Partage, Bénin"}
        subHeading={"Brevet d'Etudes du Premier Cycle (BEPC)"}
        fromDate={"2010"}
        toDate={"2014"}
      />{" "}
    </div>,

    /* WORK EXPERIENCE */
    <div className="resume-screen-container" key="work-experience">
      <div className="experience-container">
        <ResumeHeading
          heading={"Leave Smart Consulting SARL, Bénin"}
          subHeading={"DEVELOPPEUR WEB JUNIOR"}
          fromDate={"2021"}
          toDate={"2022"}
        />{" "}
        <div className="experience-description">
          <span className="resume-description-text">
            -Conception d&apos;interfaces web conviviales en HTML5, CSS3 et
            Javascript, permettant aux utilisateurs de naviguer aisément sur les
            sites, en utilisant un style simple et facilement compréhensible.{" "}
          </span>{" "}
          <br />
          <span className="resume-description-text">
            -Mise en place et rattachement d&apos;une base de données de type MySQL
            pour la persistance des informations provenant du site.{" "}
          </span>{" "}
          <br />
          <span className="resume-description-text">
            -Utilisation du Framework Laravel pour développer la partie backend
            permettant de rendre dynamique les sites.{" "}
          </span>{" "}
          <span className="resume-description-text">
            -Développement d&apos;interfaces d&apos;administration donnant ainsi au
            propriétaire de la plateforme un moyen de gestion et contrôle des
            activités menées sue le site.{" "}
          </span>{" "}
          <br />
        </div>{" "}
      </div>{" "}
    </div>,

    /* PROGRAMMING SKILLS */
    <div
      className="resume-screen-container programming-skills-container"
      key="programming-skills"
    >
      {programmingSkillsDetails.map((skill, index) => (
        <div className="skill-parent" key={index}>
          <div className="heading-bullet"> </div> <span> {skill.skill} </span>{" "}
          <div className="skill-percentage">
            <div
              style={{ width: skill.ratingPercentage + "%" }}
              className="active-percentage-bar"
            ></div>{" "}
          </div>{" "}
        </div>
      ))}{" "}
    </div>,

    /* PROJECTS */
    <div className="resume-screen-container" key="projects">
      {" "}
      {projectsDetails.map((projectsDetails, index) => (
        <ResumeHeading
          key={index}
          heading={projectsDetails.title}
          subHeading={projectsDetails.subHeading}
          description={projectsDetails.description}
          fromDate={projectsDetails.duration.fromDate}
          toDate={projectsDetails.duration.toDate}
        />
      ))}{" "}
    </div>,

    /* Interests */
    <div className="resume-screen-container" key="interests">
      <ResumeHeading
        heading="Musique"
        description="Passionné de musique classique et de jazz, je trouve mon inspiration au piano tout en participant activement à une chorale à Neuilly-Plaisance."
      />
      <ResumeHeading
        heading="Sport"
        description="Amateur de sport, passionné d'endurance et adepte de musculation, je trouve l'équilibre entre la puissance et l'endurance dans la vie comme dans mes projets de développement."
      />

      <ResumeHeading
        heading="Jeux compétitifs"
        description="J'aime beaucoup mettre mes réflexes au défi lorsque je participe à des matchs de football, ce qui m'excite le plus, c'est de progresser dans le classement et d'avoir des sessions de jeu interactives."
      />
    </div>,
  ];

  const handleCarousal = (index) => {
    let offsetHeight = 360;

    let newCarousalOffset = {
      style: { transform: "translateY(" + index * offsetHeight * -1 + "px)" },
    };

    setCarousalOffsetStyle(newCarousalOffset);
    setSelectedBulletIndex(index);
  };

  const getBullets = () => {
    return resumeBullets.map((bullet, index) => (
      <div
        onClick={() => handleCarousal(index)}
        className={
          index === selectedBulletIndex ? "bullet selected-bullet" : "bullet"
        }
        key={index}
      >
        <img
          className="bullet-logo"
          src={require(`../../assets/Resume/${bullet.logoSrc}`)}
          alt="B"
        />
        <span className="bullet-label"> {bullet.label} </span>{" "}
      </div>
    ));
  };

  const getResumeScreens = () => {
    return (
      <div
        style={carousalOffsetStyle.style}
        className="resume-details-carousal"
      >
        {resumeDetails.map((ResumeDetail) => ResumeDetail)}{" "}
      </div>
    );
  };

  useEffect(() => {
    return () => {
      /* UNSUBSCRIBE THE SUBSCRIPTIONS */
      fadeInSubscription.unsubscribe();
    };
  }, [fadeInSubscription]);

  return (
    <div
      className="resume-container screen-container fade-in"
      id={props.id || ""}
    >
      <div className="resume-content">
        <ScreenHeading title={"CV"} subHeading={"Ma biographie officielle"} />{" "}
        <div className="resume-card">
          <div className="resume-bullets">
            <div className="bullet-container">
              <div className="bullet-icons"> </div>{" "}
              <div className="bullets"> {getBullets()} </div>{" "}
            </div>{" "}
          </div>{" "}
          <div className="resume-bullet-details"> {getResumeScreens()} </div>{" "}
        </div>{" "}
      </div>{" "}
    </div>
  );
};

// Définir les types de props attendus
Resume.propTypes = {
  id: PropTypes.string, // Validez l'attribut id
  heading: PropTypes.string.isRequired, // heading est une chaîne de caractères requise
  fromDate: PropTypes.string.isRequired, // fromDate est une chaîne de caractères requise
  toDate: PropTypes.string.isRequired, // toDate est une chaîne de caractères requise
  subHeading: PropTypes.string.isRequired, // subHeading est une chaîne de caractères requise
  description: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      heading: PropTypes.string.isRequired, // Validez les propriétés de chaque élément dans items
      fromDate: PropTypes.string.isRequired,
      toDate: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
};
export default Resume;
