import React, { useState } from "react";
import PropTypes from "prop-types"; // Importez PropTypes depuis le package prop-types
import imgBack from "../../../src/images/mailz.jpeg";
import load1 from "../../../src/images/load2.gif";
import ScreenHeading from "../../utilities/screenHeading/screenHeading";
import Typical from "react-typical";
import "./ContactMe.css";
import axios from "axios";
import { toast } from "react-toastify";

const ContactMe = (props) => {
  // if (props === undefined) {
  //   return;
  // }
  // let fadeInScreenHandler = (screen) => {
  //   if (screen.fadeInScreen !== props.id) return;
  //   Animations.animations.fadeInScreen(props.id);
  // };
  // const fadeInSubscription =
  //   ScrollService.currentScreenFadeIn.subscribe(fadeInScreenHandler);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [banner, setBanner] = useState("");
  const [bool, setBool] = useState(false);

  const handleName = (e) => {
    setName(e.target.value);
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };
  const handleMessage = (e) => {
    setMessage(e.target.value);
  };

  console.log(name);

  const submitForm = async (e) => {
    e.preventDefault();

    try {
      let data = {
        name,
        email,
        message,
      };
      setBool(true);
      const res = await axios.post(`/contact`, data);
      if (name.length === 0 || email.length === 0 || message.length === 0) {
        setBanner(res.data.msg);
        toast.error(res.data.msg);
        setBool(false);
      } else if (res.status === 200) {
        setBanner(res.data.msg);
        toast.success(res.data.msg);
        setBool(false);
        setName("");
        setEmail("");
        setMessage("");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="main-container fade-in" id={props?.id || ""}>
      <ScreenHeading title={"Contact Me"} subHeading={"Restons en contact"} />
      <div className="central-form">
        <div className="col">
          <h2 className="title">
            {" "}
            <Typical loop={Infinity} steps={["Entrer en contact 📧", 1000]} />
          </h2>
          <a href="#facebook">
            <i className="fa fa-facebook-square"> </i>{" "}
          </a>{" "}
          <a href="#google">
            <i className="fa fa-google-plus-square"> </i>{" "}
          </a>{" "}
          <a href="#instagram">
            <i className="fa fa-instagram"> </i>{" "}
          </a>{" "}
          <a href="#youtube">
            <i className="fa fa-youtube-square"> </i>{" "}
          </a>{" "}
          <a href="#twitter">
            <i className="fa fa-twitter"> </i>{" "}
          </a>{" "}
        </div>
        <div className="back-form">
          <div className="img-back">
            <h4>Envoyez votre mail par ici!</h4>
            <img src={imgBack} alt="Fond de la page d'accueil" />
          </div>
          <form onSubmit={submitForm}>
            <p>{banner}</p>
            <label htmlFor="name">Nom</label>
            <input type="text" onChange={handleName} value={name} />

            <label htmlFor="email">Email</label>
            <input type="email" onChange={handleEmail} value={email} />

            <label htmlFor="message">Message</label>
            <textarea type="text" onChange={handleMessage} value={message} />

            <div className="send-btn">
              <button type="submit">
                Send<i className="fa fa-paper-plane"></i>
                {bool ? (
                  <b className="load">
                    <img src={load1} alt="Animation de chargement" />
                  </b>
                ) : (
                  ""
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

ContactMe.propTypes = {
  id: PropTypes.string.isRequired, // Ajoutez la validation ici
};

export default ContactMe;