import React from "react";
import Typical from "react-typical";
import "./Profile.css";
import ScrollService from "../../../utilities/ScrollService";

export default function Profile() {
  return (
    <div className="profile-container">
      <div className="profile-parent">
        <div className="profile-details">
          <div className="colz">
            <div className="colz-icon">
              <a href="#facebook">
                <i className="fa fa-facebook-square"> </i>{" "}
              </a>{" "}
              <a href="#google">
                <i className="fa fa-google-plus-square"> </i>{" "}
              </a>{" "}
              <a href="#instagram">
                <i className="fa fa-instagram"> </i>{" "}
              </a>{" "}
              <a href="#youtube">
                <i className="fa fa-youtube-square"> </i>{" "}
              </a>{" "}
              <a href="#twitter">
                <i className="fa fa-twitter"> </i>{" "}
              </a>{" "}
            </div>{" "}
          </div>{" "}
          <div className="profile-details-name">
            <span className="primary-text">
              {" "}
              Salut, je suis{" "}
              <span className="highlighted-text"> Beh Ardieu </span>{" "}
            </span>{" "}
          </div>{" "}
          <div className="profile-details-role">
            <span className="primary-text">
              {" "}
              <h1>
                {" "}
                <Typical
                  loop={Infinity}
                  steps={[
                    "Développeur enthousiaste 🔴",
                    1000,
                    "Développeur Full Stack 💻 ",
                    1000,
                    "Référencement SEO 😤 ",
                    1000,
                    "Développement multiplateforme ⛹ ",
                    1000,
                    "Développeur Flutter (Android/IOS) 📱 ",
                    1000,
                  ]}
                />{" "}
              </h1>{" "}
              <span className="profile-role-tagline">
                Habileté à créer des applications avec front et backend.{" "}
              </span>{" "}
            </span>{" "}
          </div>{" "}
          <div className="profile-options">
            <button
              className="btn primary-btn"
              onClick={() => ScrollService.scrollHandler.scrollToHireMe()}
            >
              {" "}
              {""}
              Engagez moi{" "}
            </button>{" "}
            <a
              href="Comlan_Beh_Ardieu_Alotin_CV.pdf"
              download="Beh Comlan_Beh_Ardieu_Alotin_CV.pdf"
            >
              <button className="btn highlighted-btn"> Obtenir mon CV </button>{" "}
            </a>{" "}
          </div>{" "}
        </div>{" "}
        <div className="profile-picture">
          <div className="profile-picture-background"> </div>{" "}
        </div>{" "}
      </div>{" "}
    </div>
  );
}
